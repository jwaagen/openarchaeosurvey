from PyQt4 import QtCore,  QtGui,  uic
from qgis import core as QgsCore
from qgis import gui as QgsGui

def snapPicture( ):
    args = ["streamer", "-o", "/tmp/newpicture.jpeg" ]
    #subprocess.Popen( args )
    picname = QtGui.QInputDialog.getText( QtGui.QInputDialog(), "Snap picture", "Name this picture" )
    if picname[1]:
        uri = os.path.join( self.projectpicturepath, str(picname[0]) )
        args = [ "mv", "/tmp/newpicture.jpeg", uri ]
        subprocess.Popen( args )
    return picname

def storePicture( plugin, picname,  x,  y ):
    reg = QgsCore.QgsMapLayerRegistry.instance()
    layer = reg.mapLayers()[ QtCore.QString( u'archeo_pictures' ) ]
    feature = QgsCore.QgsFeature()
    for i in layer.dataProvider().attributeIndexes():
        feature.addAttribute( i,  None )

    # Copy the administrative session record
    fieldNames = [ 'Campaign',  'Administrator', 'Team',  'TeamMembers',  'Weather' ]
    try:
        sessionFile = open( 'SessionConfig',  'r' )
        sessionData = sessionFile.readline().strip().split( '|-|' )
        if len( sessionData ) != len( fieldNames ):
            raise AssertionError
    except:
        sessionData = [ 'Unknown' ] * len( fieldNames )
    for i in range( len( fieldNames ) ):
        myIndex = layer.fieldNameIndex( fieldNames[i] )
        feature.addAttribute( myIndex,  sessionData[i] )
    
    # Store the picture location
    myIndex = layer.fieldNameIndex( 'PictureName' )
    feature.addAttribute( myIndex,  picname )
    
    # Store the GPS location as point geometry
    geometry = QgsCore.QgsGeometry.fromPoint( QgsCore.QgsPoint( x,  y ) )
    feature.setGeometry( geometry )
    
    # Write new feature to dataprovider
    layer.startEditing()
    layer.addFeature( feature )
    layer.commitChanges()


def actionCaptureLocationPicture( x,  y ):
        if type( x ) == float and type( y ) == float:
            picname = snapPicture( )
            if picname[1]:
                storePicture( picname[0] )
        else:
            QtGui.QMessageBox.critical( self.plugin, 'No GPS location',  'GPS position is unknown, will not be able to georeference the image! (It will not appear in your map)' )
            picname = snapPicture( )
