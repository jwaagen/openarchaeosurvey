import os
import json
import bz2
import urllib
import urllib2
from PyQt4 import QtCore,  QtGui,  uic
from qgis import core as QgsCore
from qgis import gui as QgsGui
from pyspatialite import dbapi2 as spatialite


class DataDownloadActor( QtCore.QObject ):

    def __init__( self, plugin ):
        QtCore.QObject.__init__( self )
        self.plugin = plugin

    def parseIncoming( self, compressedmessage ):
        """Unpack the compressed binary format used by the LearningSites server and construct a data container object from the message contents."""
        try:
            message = bz2.decompress( compressedmessage )
        except:
            QtGui.QMessageBox.critical( mainWindow, "Communication Error", "The data received from the server was unreadable." )
            return None
        messagedata = json.loads( message )
        return messagedata


    def applyObservationData( self , observationdata ):
        """Use ruleset for translating object notation to database update statements for the Observations"""
        pass

    def applyData( self, messagedata ):
        """Apply the given message data container to the local database."""
        if not messagedata:
            return

        connection = spatialite.connect( self.plugin.database )
        cursor = connection.cursor()

        # Add counters for reporting..
        
        count_observations = 0
        count_features = 0
        count_units = 0
        count_samples = 0
        
        # Switch off the update modification trigger, otherwise the fresh downloads will be marked as modified.
        sql = "UPDATE db_controls SET value = 0 WHERE controlname == 'CreationTriggersModification'"
        cursor.execute(sql)

        # Now follow a set of routines encoding the structure of the local
        if messagedata.has_key( 'observations' ):
            count_observations = len( messagedata['observations'] )
            for data in messagedata['observations']:
                sql = """INSERT OR REPLACE INTO archeo_observations (
                         RealID,
                         LastSync,
                         Modified,
                         Campaign,
                         Administrator,
                         Team,
                         TeamMembers,
                         Weather,
                         DateAndTime,
                         ObservationComment,
                         Geometry
                         ) VALUES (
                         %i, %i, %i,
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         GeomFromText( '%s', 4326 )
                         )
                      """ % (
                         data['id'],
                         data['lastsync'],
                         False,
                         data['fields'][0], # Campaign
                         data['fields'][1], # Administrator
                         data['fields'][2], # Team
                         data['fields'][3], # TeamMembers
                         data['fields'][4], # Weather
                         data['fields'][5], # DateAndTime
                         data['fields'][6], # ObservationComment
                         data['geom']
                      )
                cursor.execute( sql )

        if messagedata.has_key( 'features' ):
            count_features = len( messagedata['features'] )
            for data in messagedata['features']:
                sql = """INSERT OR REPLACE INTO archeo_features (
                         RealID,
                         LastSync,
                         Modified,
                         Campaign,
                         Administrator,
                         Team,
                         TeamMembers,
                         Weather,
                         DateAndTime,
                         FeatureComment,
                         Geometry
                         ) VALUES (
                         %i, %i, %i,
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         GeomFromText( '%s', 4326 )
                         )
                      """ % (
                         data['id'],
                         data['lastsync'],
                         False,
                         data['fields'][0], # Campaign
                         data['fields'][1], # Administrator
                         data['fields'][2], # Team
                         data['fields'][3], # TeamMembers
                         data['fields'][4], # Weather
                         data['fields'][5], # DateAndTime
                         data['fields'][6], # FeatureComment
                         data['geom']
                      )
                cursor.execute( sql )

        if messagedata.has_key( 'units' ):
            count_units = len( messagedata['units'] )
            for data in messagedata['units']:
                sql = """INSERT OR REPLACE INTO archeo_units (
                         RealID,
                         LastSync,
                         Modified,
                         Campaign,
                         Administrator,
                         Team,
                         TeamMembers,
                         Weather,
                         DateAndTime,
                         LocalGeomorphology,
                         SlopeClass,
                         SoilType,
                         SoilThickness,
                         SoilTexture,
                         SoilDisturbance,
                         ModernConstruction,
                         Hydrology,
                         CommentLandUse,
                         CommentArchaeology,
                         SketchMade,
                         VisitNumber,
                         SurveyCoveragePercent,
                         Stony,
                         Shady,
                         VegetationCover,
                         SoilHumidity,
                         RecentMaterial,
                         FinalVisibility,
                         PresentLandUse,
                         Tillage,
                         CommentSurveyMethod,
                         Geometry
                         ) VALUES (
                         %i, %i, %i,
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         '%s',
                         %i,
                         %i,
                         %i,
                         %i,
                         %i,
                         %i,
                         %i,
                         %i,
                         %i,
                         '%s',
                         '%s',
                         '%s',
                         GeomFromText( '%s', 4326 )
                         )
                      """ % (
                         data['id'],
                         data['lastsync'],
                         False,
                         data['fields'][0], # Campaign
                         data['fields'][1], # Administrator
                         data['fields'][2], # Team
                         data['fields'][3], # TeamMembers
                         data['fields'][4], # Weather
                         data['fields'][5], # DateAndTime
                         data['fields'][6], # LocalGeomorphology,
                         data['fields'][7], # SlopeClass,
                         data['fields'][8], # SoilType,
                         data['fields'][9], # SoilThickness,
                         data['fields'][10], # SoilTexture,
                         data['fields'][11], # SoilDisturbance,
                         data['fields'][12], # ModernConstruction,
                         data['fields'][13], # Hydrology,
                         data['fields'][14], # CommentLandUse,
                         data['fields'][15], # CommentArchaeology,
                         data['fields'][16], # SketchMade,
                         data['fields'][17], # VisitNumber,
                         data['fields'][18], # SurveyCoveragePercent,
                         data['fields'][19], # Stony,
                         data['fields'][20], # Shady,
                         data['fields'][21], # VegetationCover,
                         data['fields'][22], # SoilHumidity,
                         data['fields'][23], # RecentMaterial,
                         data['fields'][24], # FinalVisibility,
                         data['fields'][25], # PresentLandUse,
                         data['fields'][26], # Tillage,
                         data['fields'][27], # CommentSurveyMethod,
                         data['geom']
                      )
                cursor.execute( sql )
                sql = "SELECT TempID FROM archeo_units WHERE RealID == %i" % data['id']
                cursor.execute( sql )
                TempID = cursor.fetchall()[0][0]
                count_samples += len( data['samples'] )
                for sample in data['samples']:
                    sql = "INSERT INTO archeo_finds (UnitId, SampleType, NumShards) VALUES (%i, '%s', %i)" % (TempID, sample[0], sample[1])
                    cursor.execute( sql )

        # Re-enable the modification trigger.
        sql = "UPDATE db_controls SET value = 1 WHERE controlname == 'CreationTriggersModification'"
        cursor.execute(sql)

        # Refresh the map canvas, so that newly loaded features appear.
        self.plugin.interface.mapCanvas().refresh()

        # A bit of reporting, to let the user know something happened - since there may be no visible changes.
        m = "Fetched data from server:\n  %i unit(s)\n  %i sample(s)\n  %i feature(s)\n  %i observation(s)" % ( count_units, count_samples, count_features, count_observations )
        QtGui.QMessageBox.information( self.plugin.interface.mainWindow(), "Info", m )

        # Commit changes to the database and tidy up before everything goes out of scope.
        connection.commit()
        cursor.close()
        connection.close()

    def fetchDataFromServer( self, parameterdict ):

        # While testing, please leave this pointed to the fake server instance on the local host.
        mUrl = 'http://127.0.0.1/cgi-bin/fake_retreive.cgi'

        mData = urllib.urlencode( parameterdict )
        QtGui.QMessageBox.information(self.plugin.interface.mainWindow(), "Info", mData)

        req = urllib2.Request( url=mUrl, data=mData )
        response = urllib2.urlopen( req )
        responseText = response.read()
        responseData = self.parseIncoming( responseText )
        self.applyData( responseData )



class DataDownloadDialog( QtGui.QDialog ):

    def __init__( self,  plugin,  parent = None ):
        QtGui.QDialog.__init__( self, parent )
        self.plugin = plugin
        # Read interface layout from UI file
        uic.loadUi( os.path.join( self.plugin.uipath, 'DataDownloadDialog.ui'), self )
        # Prevent resizing to smaller than the minimum requirement for all content elements
        self.setMinimumSize( self.sizeHint() )
        self.actor = DataDownloadActor( self.plugin )

    def accept( self ):
        params = {"o": self.findChild( QtGui.QCheckBox,  'Observations' ).isChecked(),
                  "f": self.findChild( QtGui.QCheckBox,  'Features' ).isChecked(),
                  "u": self.findChild( QtGui.QCheckBox,  'Units' ).isChecked(),
                  "all": self.findChild( QtGui.QRadioButton, 'mAllItems' ).isChecked(),
                  "new": self.findChild( QtGui.QRadioButton, 'mNewerItems' ).isChecked(),
                  "date": self.findChild( QtGui.QCalendarWidget, 'mCalendar').selectedDate().toString("yyyy.MM.dd")
                 }
        
        self.actor.fetchDataFromServer(params)
        self.done( 1 )


class actionDownloadData( QtGui.QAction ):
        
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Fetch", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.path.join( self.plugin.iconpath, 'datadownload.png' )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )
        
    def gotClicked( self ):
        sessionParamDialog = DataDownloadDialog( self.plugin )
        sessionParamDialog.exec_()

