import os
import bz2
import urllib
import urllib2
from pyspatialite import dbapi2 as spatialite
import json
from PyQt4 import QtCore,  QtGui,  uic



class DataFormatter( object ):
    """Helper class that knows how to convert from the various spatialite table designs to a json compatible object."""
    
    def __init__( self, plugin, params ):
        self.plugin = plugin
        self.connection = spatialite.connect( self.plugin.database )
        self.cursor = self.connection.cursor()
        self.data = {"units": [], "features": [], "observations": []}
        if params['units'] is True:
            self._process_units()
        if params['features'] is True:
            self._process_features()
        if params['observations'] is True:
            self._process_observations()
    
    def _process_observations( self ):
        """Function to convert data from the archeo_observations table to json list-and-dict format."""
        sql = """SELECT RealID,
                        LastSync,
                        TempID,
                        Campaign,
                        Administrator,
                        Team,
                        TeamMembers,
                        Weather,
                        DateAndTime,
                        ObservationComment,
                        AsText(Geometry) as WKT
                 FROM archeo_observations
                 WHERE modified = 1
              """
        self.cursor.execute( sql )
        for row in self.cursor.fetchall():
            recdata = {
                       'id': row[0],
                       'lastsync': row[1],
                       'fields': list( row[2:-1] ),
                       'geom': row[-1]
                      }
            self.data['observations'].append( recdata )
    
    def _process_features( self ):
        """Function to convert data from the archeo_features table to json list-and-dict format."""
        sql = """SELECT RealID,
                        LastSync,
                        TempID,
                        Campaign,
                        Administrator,
                        Team,
                        TeamMembers,
                        Weather,
                        DateAndTime,
                        FeatureComment,
                        AsText(Geometry) as WKT
                 FROM archeo_features
                 WHERE modified = 1
              """
        self.cursor.execute( sql )
        for row in self.cursor.fetchall():
            recdata = {
                       'table': 'features',
                       'id': row[0],
                       'lastsync': row[1],
                       'fields': list( row[2:-1] ),
                       'geom': row[-1]
                      }
            self.data['features'].append( recdata )
    
    def _process_units( self ):
        """Function to convert and merge data from the archeo_units and archeo_finds tables to json list-and-dict format."""
        sql = """SELECT RealID,
                        LastSync,
                        TempID,
                        Campaign,
                        Administrator,
                        Team,
                        TeamMembers,
                        Weather,
                        DateAndTime,
                        LocalGeomorphology,
                        SlopeClass,
                        SoilType,
                        SoilThickness,
                        SoilTexture,
                        SoilDisturbance,
                        ModernConstruction,
                        Hydrology,
                        CommentLandUse,
                        CommentArchaeology,
                        SketchMade,
                        VisitNumber,
                        SurveyCoveragePercent,
                        Stony,
                        Shady,
                        VegetationCover,
                        SoilHumidity,
                        RecentMaterial,
                        FinalVisibility,
                        PresentLandUse,
                        Tillage,
                        CommentSurveyMethod,
                        AsText(Geometry) as WKT
                 FROM archeo_units
                 WHERE modified = 1
              """
        self.cursor.execute( sql )
        rows_units = self.cursor.fetchall()
        for row in rows_units:
            sql = """SELECT SampleType,
                            NumShards
                     FROM archeo_finds
                     WHERE UnitId == %i
                  """ % row[2]
            self.cursor.execute( sql )
            recdata = {
                       'id': row[0],
                       'lastsync': row[1],
                       'fields': list( row[2:-1] ),
                       'samples': self.cursor.fetchall(),
                       'geom': row[-1]
                      }
            self.data['units'].append( recdata )



class DataUploadDialog( QtGui.QDialog ):

    def __init__( self,  plugin,  parent = None ):
        QtGui.QDialog.__init__( self, parent )
        self.plugin = plugin
        # Read interface layout from UI file
        uic.loadUi( os.path.join( self.plugin.uipath, 'DataUploadDialog.ui'), self )
        # Prevent resizing to smaller than the minimum requirement for all content elements
        self.setMinimumSize( self.sizeHint() )
    
    def accept( self ):
        #self.outbound = open( '/tmp/netcommunication.log', 'w' )
        #self.outboundc = open( '/tmp/netcommunicationc.log', 'w' )

        # While testing without an actual field data server, please leave this setting to
        # point to the fake server on localhost.
        mUrl = 'http://127.0.0.1/cgi-bin/fake_recipient.cgi'

        params = { 'units': self.findChild( QtGui.QCheckBox, 'Units' ).isChecked(),
                   'features': self.findChild( QtGui.QCheckBox, 'Features' ).isChecked(),
                   'observations': self.findChild( QtGui.QCheckBox, 'Observations' ).isChecked()
                 }
        formatter = DataFormatter( self.plugin, params )
        encoder = json.JSONEncoder()
        jsondata = encoder.encode( formatter.data )
        compressionlevel = 9
        compresseddata = bz2.compress( jsondata, compressionlevel )

        # And there goes our efficiency.. if we must be able to send the compressed binary data
        # also through a GET request, then the protocol requires that the binary data is urlencoded.
        # Obviously, this is going to inflate the size of data that goes through the line.
        mData = urllib.urlencode( {"c": compresseddata} )
        
        req = urllib2.Request( url=mUrl, data=mData )
        f = urllib2.urlopen( req )
        self.handleResponse( f.read() )
        self.done( 1 )

    def handleResponse( self, response ):
        try:
            responseText = bz2.decompress( response )
            responseObject = json.loads( responseText )
        except:
            print "Unexpected return value received from server!"
            responseObject = None
        if responseObject is not None:
            connection = spatialite.connect( self.plugin.database )
            cursor = connection.cursor()

            # Switch off the update modification trigger, or we cannot reset the Modified field.
            sql = "UPDATE db_controls SET value = 0 WHERE controlname == 'TriggerModification'"
            cursor.execute(sql)

            sync = responseObject['sync']
            for key in responseObject['codes'].keys():
                for row in responseObject['codes'][key]:
                    sql = "UPDATE archeo_%s SET RealID = %i, LastSync = %i, Modified = 0 WHERE TempID == %i" % ( key, row[0], sync, row[1] )
                    #QtGui.QMessageBox.information( self.plugin.interface.mainWindow(), 'title', sql )
                    cursor.execute( sql )

            for key in responseObject['refused'].keys():
                refusedReport = ','.join( [ str(i) for i in responseObject['refused'][key] ] )
                QtGui.QMessageBox.warning( self.plugin.interface.mainWindow(), "Data refused", "Some %s data was refused because your copy of it was out of date: items %s" % (key, refusedReport ) )

            # Re-enable the modification trigger.
            sql = "UPDATE db_controls SET value = 1 WHERE controlname == 'TriggerModification'"
            cursor.execute(sql)
            connection.commit()
            cursor.close()
            connection.close()


class actionUploadData( QtGui.QAction ):
    """Represents an action/button element to be included on the plugin toolbar. This element causes the data upload dialog to be opened."""
        
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Upload", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.path.join( self.plugin.iconpath, 'dataupload.png' )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )
        
    def gotClicked( self ):
        sessionParamDialog = DataUploadDialog( self.plugin )
        sessionParamDialog.exec_()

