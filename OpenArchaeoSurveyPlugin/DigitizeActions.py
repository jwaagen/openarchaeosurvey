import os
import subprocess
from PyQt4 import QtCore, QtGui
import qgis.core as QgsCore
import qgis.gui as QgsGui

class actionDigitizeObservation( QtGui.QAction ):
    
    def __init__( self,  toolbar ):
        QtGui.QAction.__init__( self, 'Digitize Observation', None )
        self.toolbar = toolbar
        self.plugin = toolbar.plugin
        self.lsicon = QtGui.QIcon( os.path.join( self.plugin.iconpath, 'observation.png' ) )
        self.setIcon( self.lsicon )
        self.setCheckable( True )
        # Get reference to the observation data layer.
        self.layerAffinity = QgsCore.QgsMapLayerRegistry.instance().mapLayers()[ QtCore.QString(u'archeo_observations') ]
        # Get reference to the point digitizing tool.
        self.actionAffinity = self.plugin.interface.actionCapturePoint()
        # Connect the button to the custom toggle handler for this class.
        QtCore.QObject.connect( self, QtCore.SIGNAL( "toggled(bool)" ), self.gotToggled )
        
    def gotToggled( self,  checked ):
        if checked:
            # Make the observations layer active.
            self.plugin.interface.setActiveLayer( self.layerAffinity )
            # Prepare it to receive edit instructions.
            self.layerAffinity.startEditing()
            # Make the point-capture tool the current map tool in the canvas.
            self.actionAffinity.trigger()
            # Get reference to the triggered map tool instance now, for easier cleanup later.
            self.toolAffinity = self.plugin.canvas.mapTool()
            # Take out insurance against other parts of the program seizing the maptool.
            QtCore.QObject.connect( self.plugin.canvas,  QtCore.SIGNAL( "mapToolSet(QgsMapTool *)" ) ,  self.toolChanged )
        else:
            # Stop our little insurance here, or it will try to commit changes twice.
            QtCore.QObject.disconnect( self.plugin.canvas,  QtCore.SIGNAL( "mapToolSet(QgsMapTool *)" ) ,  self.toolChanged )
            # Commit, stop editing, unset digitizing map tool.
            self.layerAffinity.commitChanges()
            self.plugin.canvas.unsetMapTool( self.toolAffinity )
            # Force map to refesh once, to eliminate old vertex markers.
            self.plugin.canvas.refresh()
            
    def toolChanged( self,  maptool ):
        # Respond to external events that take control of the maptool by committing changes and setting this button inactive.
        self.layerAffinity.commitChanges()
        self.setChecked( False )
        # Force map to refesh once, to eliminate old vertex markers.
        self.plugin.canvas.refresh()

class actionDigitizeFeature( QtGui.QAction ):
        
    def __init__( self,  toolbar ):
        QtGui.QAction.__init__( self, 'Digitize Feature', None )
        self.toolbar = toolbar
        self.plugin = toolbar.plugin
        self.lsicon = QtGui.QIcon( os.path.join( self.plugin.iconpath, 'linefeature.png' ) )
        self.setIcon( self.lsicon )
        self.setCheckable( True )
        # Get reference to the archeo_features data layer.
        self.layerAffinity = QgsCore.QgsMapLayerRegistry.instance().mapLayers()[ QtCore.QString(u'archeo_features') ]
        # Get reference to the line digitizing tool.
        self.actionAffinity = self.plugin.interface.actionCaptureLine()
        # Connect the button to the custom toggle handler for this class.
        QtCore.QObject.connect( self, QtCore.SIGNAL( "toggled(bool)" ), self.gotToggled )
        
    def gotToggled( self,  checked ):
        if checked:
            # Make the archeo_features layer active.
            self.plugin.interface.setActiveLayer( self.layerAffinity )
            # Prepare it to receive edit instructions.
            self.layerAffinity.startEditing()
            # Make the point-capture tool the current map tool in the canvas.
            self.actionAffinity.trigger()
            # Get reference to the triggered map tool instance now, for easier cleanup later.
            self.toolAffinity = self.plugin.canvas.mapTool()
            # Take out insurance against other parts of the program seizing the maptool.
            QtCore.QObject.connect( self.plugin.canvas,  QtCore.SIGNAL( "mapToolSet(QgsMapTool *)" ) ,  self.toolChanged )
            # Force map to refesh once, to generate vertex markers.
            self.plugin.canvas.refresh()
        else:
            # Stop our little insurance here, or it will try to commit changes twice.
            QtCore.QObject.disconnect( self.plugin.canvas,  QtCore.SIGNAL( "mapToolSet(QgsMapTool *)" ) ,  self.toolChanged )
            # Commit, stop editing, unset digitizing map tool.
            self.layerAffinity.commitChanges()
            self.plugin.canvas.unsetMapTool( self.toolAffinity )
            # Force map to refesh once, to eliminate old vertex markers.
            self.plugin.canvas.refresh()
        
    def toolChanged( self,  maptool ):
        # Respond to external events that take control of the maptool by committing changes and setting this button inactive.
        self.layerAffinity.commitChanges()
        self.setChecked( False )
        # Force map to refesh once, to eliminate old vertex markers.
        self.plugin.canvas.refresh()

class actionDigitizeUnit( QtGui.QAction ):
    
    def __init__( self,  toolbar ):
        QtGui.QAction.__init__( self, 'Digitize Feature', None )
        self.toolbar = toolbar
        self.plugin = toolbar.plugin
        self.lsicon = QtGui.QIcon( os.path.join( self.plugin.iconpath, 'polygonfeature.png' ) )
        self.setIcon( self.lsicon )
        self.setCheckable( True )
        # Get reference to the archeo_units data layer.
        self.layerAffinity = QgsCore.QgsMapLayerRegistry.instance().mapLayers()[ QtCore.QString(u'archeo_units') ]
        # Get reference to the polygon digitizing tool.
        self.actionAffinity = self.plugin.interface.actionCapturePolygon()
        # Connect the button to the custom toggle handler for this class.
        QtCore.QObject.connect( self, QtCore.SIGNAL( "toggled(bool)" ), self.gotToggled )
        
    def gotToggled( self,  checked ):
        if checked:
            # Make the archeo_units layer active.
            self.plugin.interface.setActiveLayer( self.layerAffinity )
            # Prepare it to receive edit instructions.
            self.layerAffinity.startEditing()
            # Make the point-capture tool the current map tool in the canvas.
            self.actionAffinity.trigger()
            # Get reference to the triggered map tool instance now, for easier cleanup later.
            self.toolAffinity = self.plugin.canvas.mapTool()
            # Take out insurance against other parts of the program seizing the maptool.
            QtCore.QObject.connect( self.plugin.canvas,  QtCore.SIGNAL( "mapToolSet(QgsMapTool *)" ) ,  self.toolChanged )
            # Force map to refesh once, to generate vertex markers.
            self.plugin.canvas.refresh()
        else:
            # Stop our little insurance here, or it will try to commit changes twice.
            QtCore.QObject.disconnect( self.plugin.canvas,  QtCore.SIGNAL( "mapToolSet(QgsMapTool *)" ) ,  self.toolChanged )
            # Commit, stop editing, unset digitizing map tool.
            self.layerAffinity.commitChanges()
            self.plugin.canvas.unsetMapTool( self.toolAffinity )
            # Force map to refesh once, to eliminate old vertex markers.
            self.plugin.canvas.refresh()
            
    def toolChanged( self,  maptool ):
        # Respond to external events that take control of the maptool by committing changes and setting this button inactive.
        self.layerAffinity.commitChanges()
        self.setChecked( False )
        # Force map to refesh once, to eliminate old vertex markers.
        self.plugin.canvas.refresh()
