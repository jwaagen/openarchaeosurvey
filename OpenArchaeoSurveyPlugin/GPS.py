from PyQt4 import QtCore, QtGui
from qgis import core as QgsCore
from qgis import gui as QgsGui
import os

class GPSSelectionButton( QtGui.QPushButton ):
    def __init__( self, caller, gpsport, parent=None ):
        QtGui.QPushButton.__init__( self, parent )
        self.gpsportAddr, self.gpsportDesc = gpsport
        self.setText( self.gpsportDesc )
        self.caller = caller
        self.clicked.connect( self.handleClicked )
        
    def handleClicked( self ):
        print "GPS SELECTED:", self.gpsportAddr, self.gpsportDesc
        self.caller.plugin.detector = QgsCore.QgsGPSDetector( self.gpsportAddr )
        self.caller.plugin.detector.detected.connect( self.caller.handleDetected )
        self.caller.plugin.detector.detectionFailed.connect( self.caller.handleDetectionFailed )
        self.caller.plugin.detector.advance()
        self.caller.selectionWindow.hide()


class actionConnectGPS( QtGui.QAction ):
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Connect GPS device", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.path.join( self.plugin.iconpath, "connectgps.png" )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        self.triggered.connect( self.gotClicked )
        #QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )

    def gotClicked( self ):
        print "actionConnectGPSClicked"
        gpsList = QgsCore.QgsGPSDetector.availablePorts()
        print gpsList
        if len(gpsList) == 0:
            pass
        elif len(gpsList) == 1:
            pass
        else:
            self.selectionWindow = QtGui.QWidget()
            self.selectionWindow.setWindowTitle( "Select GPS connection port" )
            initLayout = QtGui.QVBoxLayout( self.selectionWindow )
            initLayout.addWidget( QtGui.QLabel("On which port is your GPS connected?"))
            for portInfo in gpsList:
                initLayout.addWidget( GPSSelectionButton( self, portInfo, None ) )
            self.selectionWindow.show()

    def handleDetected( self, connection ):
        self.plugin.gpsconnection = connection
        QtGui.QMessageBox.warning( self.plugin.interface.mainWindow(), "Connection Success", "Connection succeeded." )
        QtGui.QMessageBox.information( self.plugin.interface.mainWindow(), "GPS Connection Success", "GPS discovered and connected. The GPS device is now accessible to the plugin. (Position information only available when the GPS has a location fix)." )

    def handleDetectionFailed( self ):
        QtGui.QMessageBox.critical( self.plugin.interface.mainWindow(), "GPS Connection Error", "Failed to connect. No recognized gps data stream on selected port, or port not accessible." )
