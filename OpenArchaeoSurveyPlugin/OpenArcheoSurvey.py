import os
import subprocess
#import sqlite3
from PyQt4 import QtCore, QtGui
import qgis.core as QgsCore
import qgis.gui as QgsGui

# ----------------------------------------------------------------------------------------------------
#  Debug utility functions ( to be removed )
# ----------------------------------------------------------------------------------------------------

def msg( messagetext="None" ):
    msgbox = QtGui.QMessageBox()
    msgbox.setWindowTitle( "Debug" )
    msgbox.setText( messagetext )
    msgbox.exec_()

# ----------------------------------------------------------------------------------------------------
#  Dialogs
# ----------------------------------------------------------------------------------------------------

from SessionDialog import SessionManagerDialog

# ----------------------------------------------------------------------------------------------------
#  Actions
# ----------------------------------------------------------------------------------------------------

from DigitizeActions import actionDigitizeObservation

from DigitizeActions import actionDigitizeFeature

from DigitizeActions import actionDigitizeUnit

from DataUploadControl import actionUploadData

from DataDownloadControl import actionDownloadData

from GPS import actionConnectGPS


# Now follow a number of minor actions that were too small to justify branching into their own file.

class actionKeyboardTool( QtGui.QAction ):
    """Opens an onscreen keyboard for data entry on keyboardless devices."""
       
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Keyboard", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.sep.join( [ self.plugin.iconpath, "keyboard.png" ] )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )
        
    def gotClicked( self ):
        """Process the action trigger event."""
        # We are making use of the external florence keyboard, which works slightly better than the default xvkbd.
        self.pid = subprocess.Popen( ["florence"] ).pid


class actionSessionManager( QtGui.QAction ):
    """Opens custom dialog to enter persistent session data."""
        
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Session parameters", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.sep.join( [ self.plugin.iconpath, "makenotes.png" ] )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )
        
    def gotClicked( self ):
        """Process the action trigger event."""
        sessionParamDialog = SessionManagerDialog( self.plugin )
        sessionParamDialog.exec_()


class actionCommunityLauncher( QtGui.QAction ):
    """Open the UVA Community website. Either open a new browser window for this, or use an existing one (detected automatically)."""
        
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Open Community Website", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.sep.join( [ self.plugin.iconpath, "uvacommunity.png" ] )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )
        
    def gotClicked( self ):
        """Process the action trigger event."""
        self.pid = subprocess.Popen( ["x-www-browser", "http://communities.uva.nl/portal/login"] ).pid


class actionChatWindow( QtGui.QAction ):

    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Open chat window", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.sep.join( [ self.plugin.iconpath, "chat.png" ] )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )

        mainWindow = self.plugin.interface.mainWindow()
        self.myChatDock = QtGui.QDockWidget( mainWindow )
        self.myChatDock.setWindowTitle( "Chat - IRSSI" )
        self.myChatDock.setObjectName( "mChatArea" )
        mainWindow.addDockWidget( QtCore.Qt.BottomDockWidgetArea, self.myChatDock )
        self.myChatDock.hide()
        self.myChatContainer = QtGui.QX11EmbedContainer( None )
        self.myChatDock.setWidget( self.myChatContainer )
        params = ["urxvt", "-embed", str(self.myChatContainer.winId()), "-e", "irssi"]
        self.myIrssiSession = subprocess.Popen( params )
        
    def gotClicked( self ):
        """Process the action trigger event."""
        if self.myIrssiSession.poll() is not None:
            params = ["urxvt", "-embed", str(self.myChatContainer.winId()), "-e", "irssi"]
            self.myIrssiSession = subprocess.Popen( params )
        self.myChatDock.show()

    def unload( self ):
        """Cleanup for when the chat component is unloaded."""
        # Stop the chat client if running
        if self.myIrssiSession.poll() is None:
            self.myIrssiSession.terminate()
        # Remove the chat area
        mainWindow = self.plugin.interface.mainWindow()
        mainWindow.removeDockWidget( self.myChatDock )



class actionTakePicture( QtGui.QAction ):
    
    def __init__( self, toolbar ):
        QtGui.QAction.__init__( self, "Snap picture", None )
        self.toolbar = toolbar
        self.plugin = self.toolbar.plugin
        self.lsicon = os.path.join( self.plugin.iconpath, "takepicture.png" )
        self.setIcon( QtGui.QIcon( self.lsicon ) )
        QtCore.QObject.connect( self, QtCore.SIGNAL("triggered()"), self.gotClicked )

    def snapPicture( self ):
        """Spawns a subprocess to take a picture from default camera using the Streamer utility, prompts for a name and moves it to the picture path folder."""
        args = ["streamer", "-o", "/tmp/newpicture.jpeg" ]
        subprocess.Popen( args )
        picname = QtGui.QInputDialog.getText( QtGui.QInputDialog(), "Snap picture", "Name this picture" )
        if picname[1]:
            if picname[0][-5:] <> '.jpeg':
                picname = ''.join( [str(picname[0]), '.jpeg'] )
            uri = os.path.join( self.plugin.projectpicturepath, picname )
            args = [ "mv", "/tmp/newpicture.jpeg", uri ]
            subprocess.Popen( args )
            return picname
        else:
            return None

    def gotClicked( self ):
        """Process the action trigger event."""
        gps_x = 0.0
        gps_y = 0.0
        if self.plugin.gpsconnection is not None:
            try:
               gpsinfo = self.plugin.gpsconnection.currentGPSInformation()
               gps_x = gpsinfo.longitude
               gps_y = gpsinfo.latitude
            except:
               # Continue default behaviour
               pass
        if gps_x or gps_y:
            path = self.snapPicture()
            if path:
                sql = """INSERT INTO archeo_pictures ( PictureName, GeometryWKT ) VALUES (path, 'POINT(%f %f)' )"""% ( gps_x,  gps_y )
        else:
            # This theoretically _could_ trigger erroneously, if the user were standing at the exact
            # intersection of the equator and the prime meridian, with micrometer accuracy.
            QtGui.QMessageBox.critical( self.plugin.interface.mainWindow(), 'No GPS location',  'GPS position is unknown, you will not be able to geo-tag the image! (It will not appear in your map)' )
            self.snapPicture()


# ----------------------------------------------------------------------------------------------------
#  Toolbars
# ----------------------------------------------------------------------------------------------------

class LearningSitesToolBar( QtGui.QToolBar ):

    def __init__( self, plugin ):
        QtGui.QToolBar.__init__( self, "LearningSites2", None )
        # Bigger icons are easier for the touchscreen.
        self.setIconSize( QtCore.QSize( 48, 48 ) )
        self.plugin = plugin
        self.setObjectName( "mLearningSitesToolBar" )

        self.actionCommunityLauncher = actionCommunityLauncher( self )
        self.addAction( self.actionCommunityLauncher )
        self.actionKeyboardTool = actionKeyboardTool( self )
        self.addAction( self.actionKeyboardTool )
        self.actionSessionManager = actionSessionManager( self )
        self.addAction( self.actionSessionManager )
        self.actionDigitizeUnit = actionDigitizeUnit( self )
        self.addAction( self.actionDigitizeUnit )
        self.actionDigitizeFeature = actionDigitizeFeature( self )
        self.addAction( self.actionDigitizeFeature )
        self.actionDigitizeObservation = actionDigitizeObservation( self )
        self.addAction( self.actionDigitizeObservation )
        self.actionUploadData = actionUploadData( self )
        self.addAction( self.actionUploadData )
        self.actionDownloadData = actionDownloadData( self )
        self.addAction( self.actionDownloadData )
        self.actionConnectGPS = actionConnectGPS( self )
        self.addAction( self.actionConnectGPS )
        self.actionTakePicture = actionTakePicture( self )
        self.addAction( self.actionTakePicture )
        self.actionChatWindow = actionChatWindow( self )
        self.addAction( self.actionChatWindow )


# ----------------------------------------------------------------------------------------------------
#  Main plugin
# ----------------------------------------------------------------------------------------------------

class OpenArcheoSurveyPlugin( object ):
    """A proof-of-concept plugin to turn Quantum GIS into a field data gathering application.

    This plugin is academic software. Bravely developed on top of an unstable codebase, against a backdrop of vaguely defined and changing user requirements. Treat with equal parts of respect, suspicion and caution."""

    def __init__( self, iface ):
        self.interface = iface
        self.pluginpath = os.path.dirname( os.path.realpath( __file__ ) )
        self.projectpath = os.path.join( self.pluginpath,  'project' )
        self.projectpicturepath = os.path.join( self.projectpath,  'pictures' )
        self.iconpath = os.path.join( self.pluginpath,  'icons' )
        self.uipath = os.path.join( self.pluginpath,  'ui' )
        self.database = os.path.join( self.projectpath,  'surveydata.db' )
        self.gpsconnection = None
        self.canvas = iface.mainWindow().centralWidget()
        # PostInit should load out data, but only after all other initialization has finished. Queue it to run when the program says it is ready.
        self.interface.connect( self.interface, QtCore.SIGNAL( 'initializationCompleted()' ),  self.postInit )
        
    def initGui( self ):
        """Customize the QGIS interface to suit the plugin"""
        # The GUI initialization has been moved to the postInit stage, so that the tools can take references
        # to the loaded data. This is valid only because this application is data-specific, and should not be used
        # as an example for generic plugins!
        self.postInitDone = False
        
    def postInit( self ):
        """Actions that take place after all of QGis and its plugins have finished initializing"""
        # Load all data layers into the application. They are defined in a project file.
        self.interface.addProject( os.path.join( self.projectpath, 'surveydata.qgs' ) )
        # Execute delayed initGui actions.
        self.toolbar = LearningSitesToolBar( self )
        self.interface.mainWindow().addToolBar( self.toolbar )
        # Flag that we have done this
        self.postInitDone = True
        
    def unload( self ):
        """Cleans up plugin elements from the application"""
        # This should only be done if we went through postInit properly.
        if self.postInitDone:
            # Remove the embedded chat area
            self.toolbar.actionChatWindow.unload()
            # Remove the toolbar
            self.interface.mainWindow().removeToolBar( self.toolbar )
            # The current project must unloaded, because it has code associated that expects the plugin to be loaded.
            self.interface.newProject()

