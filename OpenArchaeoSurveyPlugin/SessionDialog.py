import os
from PyQt4 import QtCore, QtGui,  uic

class SessionManagerDialog( QtGui.QDialog ):
    def __init__( self, plugin ):
        QtGui.QDialog.__init__( self, None )
        self.plugin = plugin        
        # Read interface layout from UI file
        uic.loadUi( os.path.join( self.plugin.uipath, 'SessionData.ui'), self )
        # Prevent resizing to smaller than the minimum requirement for all content elements
        self.setMinimumSize( self.sizeHint() )
        
        # Copy the administrative session record to sessionData
        self.fieldNames = [ 'Campaign',  'Administrator', 'Team',  'TeamMembers',  'Weather' ]
        try:
            sessionFile = open( 'SessionConfig',  'r' )
            self.sessionData = sessionFile.readline().strip().split( '|-|' )
            if len( self.sessionData ) != len( self.fieldNames ):
                raise AssertionError
        except:
            self.sessionData = [ 'Unknown' ] * len( self.fieldNames )
            
        for i in range( len( self.fieldNames ) ):
            myField = self.findChild( QtGui.QLineEdit,  self.fieldNames[i] )
            myField.setText( self.sessionData[i] )
    def accept(self ):
        sessionOut = list()
        for i in range( len( self.fieldNames ) ):
            myField = self.findChild( QtGui.QLineEdit,  self.fieldNames[i] )
            sessionOut.append( unicode( myField.text() ) )
        with open( 'SessionConfig',  'w' ) as sessionFile:
            sessionFile.write( '|-|'.join( sessionOut ) )
        self.done( 1 )
