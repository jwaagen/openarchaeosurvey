#
# Header for the QGIS plugin manager.
#
from OpenArcheoSurvey import OpenArcheoSurveyPlugin

def name():
    return "OpenArcheoSurvey"

def description():
    return "Archaeological Survey Data Acquisition and Exchange Tool"

def version():
    return "2012/02/02"

def qgisMinimumVersion(): 
  return "1.7"

def authorName():
	return "Nils Olaf de Reus"

def icon():
    return "PluginIcon.png"

def classFactory( iface ):
    return OpenArcheoSurveyPlugin( iface )
