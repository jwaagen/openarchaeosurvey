#
# These files need to be kept in the same folder as the project file, due to restrictions in where QGis will search for them.
#

from PyQt4 import QtCore
from PyQt4 import QtGui
from qgis import core as QgsCore
from qgis import gui as QgsGui

myDialog = None

def formOpen(dialog,layerid,featureid):
    global myDialog
    myDialog = dialog
    myRegistry = QgsCore.QgsMapLayerRegistry.instance()
    myFeature = QgsCore.QgsFeature()
    myLayer = myRegistry.mapLayer( layerid )
    # QGis is right now not so good at properly pre-loading any input element other than a lineedit and textedit.
    # Code below fills in the forms with existing data when editing existing features instead of creating.
    if myLayer.featureAtId( featureid,  myFeature ):
        # Set elements of type QtGui.QDateTimeEdit
        for field in [ 'DateAndTime' ]:
            myIndex = myLayer.fieldNameIndex( field )
            myField = myDialog.findChild( QtGui.QDateTimeEdit,  field )
            val = myFeature.attributeMap()[ myIndex ].toString()
            myDateTime = QtCore.QDateTime.fromString( val, 'yyyy.MM.dd HH:mm' )
            myField.setDateTime( myDateTime )
    else:
        # This sets up useful defaults for newly created features.
        # Set the data entry time to current system time
        myField = myDialog.findChild( QtGui.QDateTimeEdit,  'DateAndTime' )
        myDateTime = QtCore.QDateTime.currentDateTime( )
        myField.setDateTime( myDateTime )
        # Copy the administrative session record
        fieldNames = [ 'Campaign',  'Administrator', 'Team',  'TeamMembers',  'Weather' ]
        try:
            sessionFile = open( 'SessionConfig',  'r' )
            sessionData = sessionFile.readline().strip().split( '|-|' )
            if len( sessionData ) != len( fieldNames ):
                raise AssertionError
        except:
            sessionData = [ 'Unknown' ] * len( fieldNames )
        for i in range( len( fieldNames ) ):
            myField = myDialog.findChild( QtGui.QLineEdit,  fieldNames[i] )
            myField.setText( sessionData[i] )
    # QGis has a bad habit of enabling all fields it touches, but session fields
    # should be disabled until their group box is checked. The quickest way to
    # get everything in the right state again is to toggle the group box on and off again..
    myField = myDialog.findChild( QtGui.QGroupBox,  'SessionBox' )
    myField.setChecked( True )
    myField.setChecked( False )
    # The session tab gains focus because of initiliazation order, so return focus to the observation tab.
    myTab = myDialog.findChild( QtGui.QWidget,  'mObservationTab' )
    myDialog.findChild( QtGui.QTabWidget,  'tabWidget' ).setCurrentWidget( myTab )
