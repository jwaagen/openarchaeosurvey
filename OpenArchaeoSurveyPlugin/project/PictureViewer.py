#
# These files need to be kept in the same folder as the project file, due to restrictions in where QGis will search for them.
#

import os
from PyQt4 import QtCore
from PyQt4 import QtGui
from qgis import core as QgsCore
from qgis import gui as QgsGui

myDialog = None

def formOpen(dialog,layerid,featureid):
    global myDialog
    myDialog = dialog
    myFolder = os.path.abspath( os.path.dirname( __file__ ) )
    myPictureFolder = os.path.join( myFolder,  'pictures' )
    myRegistry = QgsCore.QgsMapLayerRegistry.instance()
    myFeature = QgsCore.QgsFeature()
    myLayer = myRegistry.mapLayer( layerid )
    # QGis is right now not so good at properly pre-loading any input element other than a lineedit and textedit.
    # Code below fills in the forms with existing data when editing existing features instead of creating.
    if myLayer.featureAtId( featureid,  myFeature ):
        # Find picture PictureName from the record and put the image on the widget.
        myIndex = myLayer.fieldNameIndex( 'PictureName' )
        myField = myDialog.findChild( QtGui.QLabel,  'picture' )
        # Note that toString produces a QtCore.QString object, not a Python string. Hence the explicit cast as str( ... )
        picName = str( myFeature.attributeMap()[ myIndex ].toString() )
        picPath = os.path.join( myPictureFolder, picName )
        myField.setPixmap( QtGui.QPixmap( picPath ) )
        myDialog.setMinimumSize( myDialog.sizeHint() )
        myDialog.resize( myDialog.sizeHint() )
    else:
        # This form is not used for the creation of new features.
        pass
