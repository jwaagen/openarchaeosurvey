# This file contains a few common tools and convenience methods that are not native part of Python 2.6


# Define a reference to the type of an instance method. This is a largely
# undocumented feature, because it is a class of the __builtin__ module but
# not directly accessible.

class dummy( object ):
    def dummy( self ):
        pass

instancemethod = type( dummy.dummy )
