#
# These files need to be kept in the same folder as the project file, due to restrictions in where QGis will search for them.
#

from PyQt4 import QtCore, QtGui, uic
from qgis import core as QgsCore
from qgis import gui as QgsGui
from qgis import utils as QgsUtils
from pyspatialite import dbapi2 as spatialite
from ToolKit import instancemethod
import os

myDialog = None
myLayer = None
myFeature = None

class FindsListItem( QtGui.QListWidgetItem ):
    def __init__( self, record, text, parent = None ):
       QtGui.QListWidgetItem.__init__( self, text, parent, 1001 )
       self.oid, self.UnitId, self.SampleType, self.NumShards = record 

def repopulateSamples( self ):
    global myLayer
    global myFeature
    myPlugin = QgsUtils.plugins['LS2012']
    myIndex = myLayer.fieldNameIndex( 'TempID' )
    self.myUnitID = myFeature.attributeMap()[ myIndex ].toInt()[0]
    myField = myDialog.findChild( QtGui.QListWidget, 'sampleList' )
    sql = "SELECT TempID, UnitId, SampleType, NumShards FROM archeo_finds WHERE UnitId = %i" % (self.myUnitID)
    self.dbcursor.execute( sql )
    myField.clear()
    for record in self.dbcursor.fetchall():
       myField.addItem( FindsListItem( record, "Sample: %s, %i pieces." % ( record[2], record[3] ) ) )

def deleteSample( self ):
    myField = self.findChild( QtGui.QListWidget, 'sampleList' )
    sample = myField.currentItem()
    if sample:
        sql = "DELETE FROM archeo_finds WHERE TempID = %i" % sample.oid
        self.dbcursor.execute( sql )
        self.dbconnection.commit()
        self.repopulateSamples()
 
def addSample( self ):
    if self.myUnitID:
        myField = self.findChild( QtGui.QListWidget, 'sampleList' )
        sampleDialog = QtGui.QDialog()
        uic.loadUi( os.path.join( self.plugin.uipath, 'SampleDataEntry.ui'), sampleDialog )
        sampleDialog.exec_()
        type = unicode(sampleDialog.findChild( QtGui.QComboBox, 'SampleType' ).currentText())
        numshards = int(sampleDialog.findChild( QtGui.QSpinBox, 'NumShards').value())
        sql = "INSERT INTO archeo_finds ( UnitId, SampleType, NumShards ) VALUES ( %i, \"%s\", %i )" % ( self.myUnitID, type, numshards )
        self.dbcursor.execute( sql )
        self.dbconnection.commit()
        self.repopulateSamples()
    else:
        QtGui.QMessageBox.warning( self, "Notice", "Please finalize and Ok the unit before adding samples. Samples can be added by selecting a unit with the Information tool." )
    
 
def formOpen(dialog,layerid,featureid):
    global myDialog
    global myLayer
    global myFeature
    myPlugin = QgsUtils.plugins['LS2012']
    myDialog = dialog
    myRegistry = QgsCore.QgsMapLayerRegistry.instance()
    myFeature = QgsCore.QgsFeature()
    myLayer = myRegistry.mapLayer( layerid )
    myDialog.plugin = myPlugin
    myDialog.myUnitID = None
    myDialog.repopulateSamples = instancemethod( repopulateSamples, myDialog, myDialog.__class__ )
    myDialog.deleteSample = instancemethod( deleteSample, myDialog, myDialog.__class__ )
    myDialog.addSample = instancemethod( addSample, myDialog, myDialog.__class__ )

    # Connect the buttons..

    myButton = myDialog.findChild( QtGui.QPushButton, 'addSample' )
    myButton.connect( myButton, QtCore.SIGNAL("pressed()"), myDialog.addSample )
    myButton = myDialog.findChild( QtGui.QPushButton, 'editSample' )
    myButton = myDialog.findChild( QtGui.QPushButton, 'deleteSample' )
    myButton.connect( myButton, QtCore.SIGNAL("pressed()"), myDialog.deleteSample )

    # QGis is right now not so good at properly pre-loading any input element other than a lineedit and textedit.
    # Code below fills in the forms with existing data when editing existing features instead of creating.

    if myLayer.featureAtId( featureid,  myFeature ):

        # Set elements of type QtGui.QSlider
        for field in [ 'Stony',  'Shady',  'VegetationCover',  'SoilHumidity',  'RecentMaterial',  'FinalVisibility' ]:
            myIndex = myLayer.fieldNameIndex( field )
            myField = myDialog.findChild( QtGui.QSlider,  field )
            val,  ok = myFeature.attributeMap()[ myIndex ].toUInt()
            if ok:
                myField.setValue( val )

        # Set elements of type QtGui.QComboBox
        for field in [ 'LocalGeomorphology', 'PresentLandUse',  'Tillage',  'SlopeClass',  'SoilType',  'SoilThickness' ,  'SoilTexture',  'SoilDisturbance',  'ModernConstruction',  'Hydrology', 'SurveyCoveragePercent' ]:
            myIndex = myLayer.fieldNameIndex( field )
            myField = myDialog.findChild( QtGui.QComboBox,  field )
            val = myFeature.attributeMap()[ myIndex ].toString()
            myField.insertItem( 0, val )
            myField.setCurrentIndex( 0 )

        # Set elements of type QtGui.QCheckBox
        for field in [ 'SketchMade' ]:
            myIndex = myLayer.fieldNameIndex( field )
            myField = myDialog.findChild( QtGui.QCheckBox,  field )
            val = myFeature.attributeMap()[ myIndex ].toBool()
            myField.setChecked( val )

        # Set elements of type QtGui.QDateTimeEdit
        for field in [ 'DateAndTime' ]:
            myIndex = myLayer.fieldNameIndex( field )
            myField = myDialog.findChild( QtGui.QDateTimeEdit,  field )
            val = myFeature.attributeMap()[ myIndex ].toString()
            myDateTime = QtCore.QDateTime.fromString( val, 'yyyy.MM.dd HH:mm' )
            myField.setDateTime( myDateTime )

        # Construct the identity label of this item from its real and local id
        myIndex = myLayer.fieldNameIndex( 'RealID' )
        valreal, ok = myFeature.attributeMap()[ myIndex ].toUInt()
        if not ok:
            valreal = '--'
        myIndex = myLayer.fieldNameIndex( 'TempID' )
        valtemp, ok = myFeature.attributeMap()[ myIndex ].toUInt()
        myField = myDialog.findChild( QtGui.QLabel, 'mDisplayID' )
        myField.setText( "%s / %i" % (valreal, valtemp) )

        # The list of samples must be imported from a different table entirely. For neatness, this has been isolated into a function of its own.
        myDialog.dbconnection = spatialite.connect( myPlugin.database )
        myDialog.dbcursor = myDialog.dbconnection.cursor()
        myDialog.repopulateSamples()
    else:

        # This sets up useful defaults for newly created features.
        # Set the data entry time to current system time
        myField = myDialog.findChild( QtGui.QDateTimeEdit,  'DateAndTime' )
        myDateTime = QtCore.QDateTime.currentDateTime( )
        myField.setDateTime( myDateTime )

        # Copy the administrative session record
        fieldNames = [ 'Campaign',  'Administrator', 'Team',  'TeamMembers',  'Weather' ]
        try:
            sessionFile = open( 'SessionConfig',  'r' )
            sessionData = sessionFile.readline().strip().split( '|-|' )
            if len( sessionData ) != len( fieldNames ):
                raise AssertionError
        except:
            sessionData = [ 'Unknown' ] * len( fieldNames )
        for i in range( len( fieldNames ) ):
            myField = myDialog.findChild( QtGui.QLineEdit,  fieldNames[i] )
            myField.setText( sessionData[i] )

    # QGis has a default of enabling all fields it touches, but session fields
    # should be disabled until their group box is checked. The quickest way to
    # get everything in the right state again is to toggle the group box on and off again..
    myField = myDialog.findChild( QtGui.QGroupBox,  'SessionBox' )
    myField.setChecked( True )
    myField.setChecked( False )

    # The finds tab gains focus because of initiliazation order, so return focus to the first tab.
    myTab = myDialog.findChild( QtGui.QWidget,  'mIdentityTab' )
    myDialog.findChild( QtGui.QTabWidget,  'tabWidget' ).setCurrentWidget( myTab )
