<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="1.7.0-Wroclaw" minimumScale="0" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <transparencyLevelInt>255</transparencyLevelInt>
  <renderer-v2 symbollevels="0" type="singleSymbol">
    <symbols>
      <symbol outputUnit="MM" alpha="1" type="fill" name="0">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="color" v="137,170,137,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="no"/>
          <prop k="width_border" v="0.26"/>
        </layer>
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="color" v="0,170,0,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="style" v="b_diagonal"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
    </symbols>
    <rotation field=""/>
    <sizescale field=""/>
  </renderer-v2>
  <customproperties/>
  <displayfield>SoilHumidity</displayfield>
  <label>0</label>
  <labelattributes>
    <label fieldname="" text="Label"/>
    <family fieldname="" name="Sans Serif"/>
    <size fieldname="" units="pt" value="12"/>
    <bold fieldname="" on="0"/>
    <italic fieldname="" on="0"/>
    <underline fieldname="" on="0"/>
    <strikeout fieldname="" on="0"/>
    <color fieldname="" red="0" blue="0" green="0"/>
    <x fieldname=""/>
    <y fieldname=""/>
    <offset x="0" y="0" units="pt" yfieldname="" xfieldname=""/>
    <angle fieldname="" value="0" auto="0"/>
    <alignment fieldname="" value="center"/>
    <buffercolor fieldname="" red="255" blue="255" green="255"/>
    <buffersize fieldname="" units="pt" value="1"/>
    <bufferenabled fieldname="" on=""/>
    <multilineenabled fieldname="" on=""/>
    <selectedonly on=""/>
  </labelattributes>
  <edittypes>
    <edittype type="0" name="Administrator"/>
    <edittype type="0" name="Campaign"/>
    <edittype type="0" name="CommentArchaeology"/>
    <edittype type="0" name="CommentLandUse"/>
    <edittype type="0" name="CommentSurveyMethod"/>
    <edittype type="0" name="DateAndTime"/>
    <edittype type="0" name="FinalVisibility"/>
    <edittype type="0" name="Hydrology"/>
    <edittype type="0" name="LastSync"/>
    <edittype type="0" name="LocalGeomorphology"/>
    <edittype type="0" name="ModernConstruction"/>
    <edittype type="0" name="Modified"/>
    <edittype type="0" name="PresentLandUse"/>
    <edittype type="0" name="RealID"/>
    <edittype type="0" name="RecentMaterial"/>
    <edittype type="0" name="Shady"/>
    <edittype type="0" name="SketchMade"/>
    <edittype type="0" name="SlopeClass"/>
    <edittype type="0" name="SoilDisturbance"/>
    <edittype type="0" name="SoilHumidity"/>
    <edittype type="0" name="SoilTexture"/>
    <edittype type="0" name="SoilThickness"/>
    <edittype type="0" name="SoilType"/>
    <edittype type="0" name="Stony"/>
    <edittype type="0" name="SurveyCoveragePercent"/>
    <edittype type="0" name="Team"/>
    <edittype type="0" name="TeamMembers"/>
    <edittype type="0" name="TempID"/>
    <edittype type="0" name="Tillage"/>
    <edittype type="0" name="VegetationCover"/>
    <edittype type="0" name="VisitNumber"/>
    <edittype type="0" name="Weather"/>
  </edittypes>
  <editform>.</editform>
  <editforminit></editforminit>
  <annotationform>.</annotationform>
  <attributeactions/>
</qgis>
